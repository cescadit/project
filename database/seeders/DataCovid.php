<?php

namespace Database\Seeders;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DataCovid extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('datacovids')->insert([
            'id' =>4,
            'provinsi' => 'Jawa Tengan',
            'dirawat' => 236,
            'terkonfirmasi' => 627.619,
            'sembuh' => 594.093,
            'meninggal' =>33.290,
            
        ]);
    }
}
