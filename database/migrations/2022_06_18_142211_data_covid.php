<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datacovids', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('provinsi');
            $table->string('dirawat');
            $table->string('terkonfirmasi');
            $table->string('sembuh');
            $table->string('meninggal');




            $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
