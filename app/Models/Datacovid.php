<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Datacovid extends Model
{
    //
    protected $fillable = [
        'id', 'provinsi', 'dirawat', 'terkonfirmasi','sembuh','meninggal'
    ];

    public function comments()
    {
        return $this->hasMany(datacovids::class);
    }
}
